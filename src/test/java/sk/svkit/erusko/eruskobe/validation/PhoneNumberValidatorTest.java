package sk.svkit.erusko.eruskobe.validation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PhoneNumberValidatorTest {

    @Test
    public void validationSuccessTest() {
        PhoneNumberValidator validator = new PhoneNumberValidator();
        boolean valid = validator.isValid("+421905555555", null);
        assertTrue(valid);

        valid = validator.isValid("0905555555", null);
        assertTrue(valid);

        valid = validator.isValid("+421556789123", null);
        assertTrue(valid);

        valid = validator.isValid("055 6789123", null);
        assertTrue(valid);
    }

    @Test
    public void validationFailedTest() {
        PhoneNumberValidator validator = new PhoneNumberValidator();
        boolean valid = validator.isValid(null, null);
        assertFalse(valid);

        valid = validator.isValid("sdfgsg", null);
        assertFalse(valid);

        valid = validator.isValid("456", null);
        assertFalse(valid);

        valid = validator.isValid("+", null);
        assertFalse(valid);
    }
}
