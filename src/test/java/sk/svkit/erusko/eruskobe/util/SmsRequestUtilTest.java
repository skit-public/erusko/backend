package sk.svkit.erusko.eruskobe.util;

import org.junit.jupiter.api.Test;
import sk.svkit.erusko.eruskobe.service.smsgateway.SmsDataDto;
import sk.svkit.erusko.eruskobe.service.smsgateway.request.SmsRequest;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.*;

class SmsRequestUtilTest {

    @Test
    void createSmsRequest() throws ParseException {
        SmsDataDto data = new SmsDataDto();
        data.setPhoneNumber("+421999999");
        data.setVerificationCode("VC123456");
        data.setFirstname(" J");
        data.setLastname("S f(*&*&8978)");
        data.setBirthdayYear(null);
        data.setVerificationCodeExpiration(new SimpleDateFormat("dd/MM/yyyy HH:mm").parse("31/12/2020 23:50"));
        String smsTemplate =
                "{NAME_FIRST_CHARACTER},{SURNAME_FIRST_CHARACTER}, {BIRTHDATE_YEAR}, {VERIFICATION_CODE}, {VERIFICATION_CODE_EXPIRY_DATE}";
        String smsGatewayApiKey = "api key value";

        SmsRequest result = SmsRequestUtil.createSmsRequest(data, smsTemplate, smsGatewayApiKey, "UVZ");

        assertNotNull(result);
        assertEquals(result.getApiKey().getApiKey(), "api key value");
        assertNotNull(result.getSmsRequestData().getSmsRecipients());
        assertTrue(result.getSmsRequestData().getMessage().contains("23:50"));
        assertFalse(result.getSmsRequestData().getMessage().contains("{"));
        assertFalse(result.getSmsRequestData().getMessage().contains("}"));
        assertEquals(result.getSmsRequestData().getSmsSender().getText(), "UVZ");
    }

    @Test
    void sendEmptyData() {
        SmsDataDto data = new SmsDataDto();
        SmsRequest result = SmsRequestUtil.createSmsRequest(data, "", "", "");
        assertNotNull(result);
        //check default set
        assertNotNull(result.getSmsRequestData().getSmsSender());
        assertEquals(result.getSmsRequestData().getSmsSender().getText(), "UVZ");

        SmsRequest result2 = SmsRequestUtil.createSmsRequest(data, "", null, null);
        assertNotNull(result2);
        //check default set
        assertNotNull(result2.getSmsRequestData().getSmsSender());
        assertEquals(result2.getSmsRequestData().getSmsSender().getText(), "UVZ");
    }
}

