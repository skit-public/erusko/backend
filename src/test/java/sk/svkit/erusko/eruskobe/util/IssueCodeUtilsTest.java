package sk.svkit.erusko.eruskobe.util;

import org.junit.jupiter.api.Test;
import sk.svkit.erusko.eruskobe.service.ErrorType;
import sk.svkit.erusko.eruskobe.service.issuecode.IssueCodeDto;
import sk.svkit.erusko.eruskobe.service.issuecode.IssueCodeRequest;
import sk.svkit.erusko.eruskobe.service.issuecode.IssueCodeResponse;
import sk.svkit.erusko.eruskobe.service.issuecode.VerificationCode;

import java.util.Date;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class IssueCodeUtilsTest {

    @Test
    public void testConvertIssueCodeDto2IssueCodeRequest() {

        IssueCodeDto issueCodeDto = new IssueCodeDto();
        issueCodeDto.setSymptomDate("2020-10-20");
        issueCodeDto.setTestDate("2020-10-21");
        issueCodeDto.setTestType("testType");
        issueCodeDto.setTzOffset(10);

        IssueCodeRequest issueCodeRequest = IssueCodeUtils.convertIssueCodeDto2IssueCodeRequest(issueCodeDto);
        assertNotNull(issueCodeRequest);
        assertEquals("2020-10-20", issueCodeRequest.getSymptomDate());
        assertEquals("2020-10-21", issueCodeRequest.getTestDate());
        assertEquals("testType", issueCodeRequest.getTestType());
        assertEquals(10, issueCodeRequest.getTzOffset());
        assertNotNull(issueCodeRequest.getPadding());
    }

    @Test
    public void testConvertIssueCodeResponse2VerificationCode() {
        Date expiresAt = new Date(1604316987889L);

        UUID uuid = UUID.randomUUID();
        IssueCodeResponse issueCodeResponse = new IssueCodeResponse();
        issueCodeResponse.setUuid(uuid);
        issueCodeResponse.setCode("verificationCode");
        issueCodeResponse.setLongCode("longVerificationCode");
        issueCodeResponse.setExpiresAt(expiresAt);

        VerificationCode verificationCode = IssueCodeUtils.convertIssueCodeResponse2VerificationCode(issueCodeResponse);
        assertNotNull(verificationCode);
        assertEquals(uuid, verificationCode.getUuid());
        assertEquals("longVerificationCode", verificationCode.getCode());
        assertEquals(expiresAt, verificationCode.getExpiresAt());
        assertNull(verificationCode.getErrorCode());
        assertNull(verificationCode.getError());

        //error
        issueCodeResponse.setError("error");
        issueCodeResponse.setErrorCode("errorCode");

        verificationCode = IssueCodeUtils.convertIssueCodeResponse2VerificationCode(issueCodeResponse);
        assertNotNull(verificationCode);
        assertEquals(ErrorType.VC.getPrefix() + "error", verificationCode.getError());
        assertEquals(ErrorType.VC.getCode() + "errorCode", verificationCode.getErrorCode());
    }
}
