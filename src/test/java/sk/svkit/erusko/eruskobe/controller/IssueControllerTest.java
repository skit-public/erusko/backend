package sk.svkit.erusko.eruskobe.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import sk.svkit.erusko.eruskobe.controller.dto.IssueApiRequest;
import sk.svkit.erusko.eruskobe.service.ErrorType;
import sk.svkit.erusko.eruskobe.service.issuecode.VerificationCode;
import sk.svkit.erusko.eruskobe.service.issuecode.VerificationCodeIssuingService;
import sk.svkit.erusko.eruskobe.service.smsgateway.SmsGatewayService;
import sk.svkit.erusko.eruskobe.service.smsgateway.SmsResultDto;

import java.util.Date;
import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(IssuingController.class)
public class IssueControllerTest {

    private static final ObjectWriter WRITER = new ObjectMapper().writer();

    @Autowired
    private MockMvc mvc;

    @MockBean
    private VerificationCodeIssuingService verificationCodeIssuingService;

    @MockBean
    private SmsGatewayService smsGatewayService;

    @Test
    public void testIssueSuccess() throws Exception {

        IssueApiRequest request = new IssueApiRequest();
        request.setPhone("+420905555555");

        VerificationCode verificationCode = new VerificationCode();
        verificationCode.setCode("verificationCode");
        UUID codeUuid = UUID.randomUUID();
        verificationCode.setUuid(codeUuid);
        Date expiresAt = new Date();
        verificationCode.setExpiresAt(expiresAt);

        SmsResultDto smsResultDto = new SmsResultDto();
        smsResultDto.setBatchId("batchId");

        doReturn(verificationCode).when(verificationCodeIssuingService).issueCode(any());
        doReturn(smsResultDto).when(smsGatewayService).sendSMS(any());

        mvc.perform(MockMvcRequestBuilders.post("/api/issue")
                .contentType(MediaType.APPLICATION_JSON)
                .header(IssuingController.API_KEY_HEADER, "apiKey")
                .content(WRITER.writeValueAsBytes(request)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.codeUuid", is(codeUuid.toString())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.smsBatchId", is("batchId")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.error").doesNotExist())
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorCode").doesNotExist());
    }

    @Test
    public void testIssueVerificationServerIssueFailed() throws Exception {

        IssueApiRequest request = new IssueApiRequest();
        request.setPhone("+420905555555");

        VerificationCode verificationCode = new VerificationCode();
        verificationCode.setErrorCode(ErrorType.VC.getCode());
        verificationCode.setError("error");

        doReturn(verificationCode).when(verificationCodeIssuingService).issueCode(any());

        mvc.perform(MockMvcRequestBuilders.post("/api/issue")
                .contentType(MediaType.APPLICATION_JSON)
                .header(IssuingController.API_KEY_HEADER, "apiKey")
                .content(WRITER.writeValueAsBytes(request)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.codeUuid").doesNotExist())
                .andExpect(MockMvcResultMatchers.jsonPath("$.smsBatchId").doesNotExist())
                .andExpect(MockMvcResultMatchers.jsonPath("$.error", is("error")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorCode", is(ErrorType.VC.getCode())));
    }

    @Test
    public void testIssueSendSmsFailed() throws Exception {

        IssueApiRequest request = new IssueApiRequest();
        request.setPhone("+420905555555");

        VerificationCode verificationCode = new VerificationCode();
        verificationCode.setCode("verificationCode");
        UUID codeUuid = UUID.randomUUID();
        verificationCode.setUuid(codeUuid);
        Date expiresAt = new Date();
        verificationCode.setExpiresAt(expiresAt);

        SmsResultDto smsResultDto = new SmsResultDto();
        smsResultDto.setError("error");
        smsResultDto.setErrorCode(ErrorType.SG.getCode());

        doReturn(verificationCode).when(verificationCodeIssuingService).issueCode(any());
        doReturn(smsResultDto).when(smsGatewayService).sendSMS(any());

        mvc.perform(MockMvcRequestBuilders.post("/api/issue")
                .contentType(MediaType.APPLICATION_JSON)
                .header(IssuingController.API_KEY_HEADER, "apiKey")
                .content(WRITER.writeValueAsBytes(request)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.codeUuid", is(codeUuid.toString())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.smsBatchId").doesNotExist())
                .andExpect(MockMvcResultMatchers.jsonPath("$.error", is("error")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorCode", is(ErrorType.SG.getCode())));
    }

    @Test
    public void testIssueValidationFailed() throws Exception {

        IssueApiRequest request = new IssueApiRequest();
        request.setPhone("sdfghs");

        doThrow(new RuntimeException("error")).when(verificationCodeIssuingService).issueCode(any());

        mvc.perform(MockMvcRequestBuilders.post("/api/issue")
                .contentType(MediaType.APPLICATION_JSON)
                .header(IssuingController.API_KEY_HEADER, "apiKey")
                .content(WRITER.writeValueAsBytes(request)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.error", is("phone: Format not valid")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorCode", is(ErrorType.BAD_REQUEST.getCode())));
    }

    @Test
    public void testIssueInternalError() throws Exception {

        IssueApiRequest request = new IssueApiRequest();
        request.setPhone("+420905555555");

        doThrow(new RuntimeException("error")).when(verificationCodeIssuingService).issueCode(any());

        mvc.perform(MockMvcRequestBuilders.post("/api/issue")
                .contentType(MediaType.APPLICATION_JSON)
                .header(IssuingController.API_KEY_HEADER, "apiKey")
                .content(WRITER.writeValueAsBytes(request)))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.error", is("error")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorCode", is(ErrorType.INTERNAL_ERROR.getCode())));
    }
}
