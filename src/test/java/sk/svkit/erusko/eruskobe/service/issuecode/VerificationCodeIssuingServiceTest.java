package sk.svkit.erusko.eruskobe.service.issuecode;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import sk.svkit.erusko.eruskobe.service.ErrorType;

import java.nio.charset.Charset;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class VerificationCodeIssuingServiceTest {

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private VerificationCodeIssuingService verificationCodeIssuingService;

    @Test
    public void testIssueCodeSuccess() {
        IssueCodeResponse response = new IssueCodeResponse();
        response.setLongCode("longVerificationCode");
        doReturn(response).when(restTemplate).postForObject(endsWith("/api/issue"), any(HttpEntity.class), eq(IssueCodeResponse.class));

        VerificationCode verificationCode = verificationCodeIssuingService.issueCode(new IssueCodeDto());
        assertNotNull(verificationCode);
        assertEquals("longVerificationCode", verificationCode.getCode());
    }

    @Test
    public void testIssueCodeHttpClientErrorWithBody() throws JsonProcessingException {
        IssueCodeResponse issueCodeResponse = new IssueCodeResponse();
        issueCodeResponse.setError("error");

        ObjectWriter writer = new ObjectMapper().writer();

        HttpClientErrorException badRequest = new HttpClientErrorException(HttpStatus.BAD_REQUEST,
                "Bad Request", writer.writeValueAsBytes(issueCodeResponse), Charset.defaultCharset());
        doThrow(badRequest).when(restTemplate).postForObject(endsWith("/api/issue"), any(HttpEntity.class),
                eq(IssueCodeResponse.class));

        VerificationCode verificationCode = verificationCodeIssuingService.issueCode(new IssueCodeDto());
        assertNotNull(verificationCode);
        assertEquals(ErrorType.VC.getPrefix() + "error", verificationCode.getError());
        assertEquals(ErrorType.VC.getCode(), verificationCode.getErrorCode());
    }

    @Test
    public void testIssueCodeHttpClientErrorWithoutBody() {
        HttpClientErrorException badRequest = new HttpClientErrorException(HttpStatus.UNAUTHORIZED,
                "Unauthorized", null, Charset.defaultCharset());
        doThrow(badRequest).when(restTemplate).postForObject(endsWith("/api/issue"), any(HttpEntity.class),
                eq(IssueCodeResponse.class));

        VerificationCode verificationCode = verificationCodeIssuingService.issueCode(new IssueCodeDto());
        assertNotNull(verificationCode);
        assertEquals(ErrorType.VC.getPrefix() + "Unauthorized", verificationCode.getError());
        assertEquals(ErrorType.VC.getCode(), verificationCode.getErrorCode());
    }

    @Test
    public void testIssueCodeHttpClientErrorBodyDeserializationFails() {
        HttpClientErrorException badRequest = new HttpClientErrorException(HttpStatus.BAD_REQUEST,
                "Bad Request", "blabla".getBytes(), Charset.defaultCharset());
        doThrow(badRequest).when(restTemplate).postForObject(endsWith("/api/issue"), any(HttpEntity.class),
                eq(IssueCodeResponse.class));

        VerificationCode verificationCode = verificationCodeIssuingService.issueCode(new IssueCodeDto());
        assertNotNull(verificationCode);
        assertEquals(ErrorType.VC.getPrefix() + "Bad Request", verificationCode.getError());
        assertEquals(ErrorType.VC.getCode(), verificationCode.getErrorCode());
    }
}
