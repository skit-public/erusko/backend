package sk.svkit.erusko.eruskobe.service.smsgateway;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;
import sk.svkit.erusko.eruskobe.service.smsgateway.response.SmsResponseData;
import sk.svkit.erusko.eruskobe.service.smsgateway.response.SmsResponse;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = {"spring.main.allow-bean-definition-overriding=true"})
@TestPropertySource(locations = {"classpath:application.properties"})
class SmsGatewayServiceTest {

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private SmsGatewayService service;

    @Test
    void sendSMSSuccess() {
        SmsResponse response = new SmsResponse();
        response.setId("OK");
        SmsResponseData rd = new SmsResponseData();
        rd.setBatchId("123456");
        response.setData(rd);
        Mockito.doReturn(response).when(restTemplate).postForObject(endsWith("/send_batch"), any(HttpEntity.class), eq(SmsResponse.class));
        SmsDataDto smsDataDto = new SmsDataDto();
        smsDataDto.setBirthdayYear(1986);

        SmsResultDto smsResultDto = service.sendSMS(smsDataDto);
        assertNotNull(smsResultDto);
        assertNotNull(smsResultDto.getBatchId());
    }
}

