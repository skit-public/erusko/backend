package sk.svkit.erusko.eruskobe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ErBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(ErBackendApplication.class, args);
    }

}
