package sk.svkit.erusko.eruskobe.event;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import sk.svkit.erusko.eruskobe.service.GoogleAnalyticsSendService;

@Component
public class AnalyticsEventListener {
    private static final Log LOGGER = LogFactory.getLog(AnalyticsEventListener.class);

    @Autowired
    private GoogleAnalyticsSendService googleAnalyticsSendService;

    @Async
    @EventListener
    void publishAnalyticsData(AnalyticsEvent event) {
        googleAnalyticsSendService.sendData(event.getType());
    }
}
