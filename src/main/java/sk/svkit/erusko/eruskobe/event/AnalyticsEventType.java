package sk.svkit.erusko.eruskobe.event;

public enum AnalyticsEventType {
    server_vc_request,
    server_vc_success,
    server_vc_error
}
