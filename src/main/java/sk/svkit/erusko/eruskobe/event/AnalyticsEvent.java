package sk.svkit.erusko.eruskobe.event;

public class AnalyticsEvent {
    private String type;

    public AnalyticsEvent(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
