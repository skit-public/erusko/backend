package sk.svkit.erusko.eruskobe.service;

public enum ErrorType {
    BAD_REQUEST("400"),
    INTERNAL_ERROR("500"),
    VC("400.1", "Verification server: "),
    SG("400.2", "Sms gateway: ");

    private final String code;
    private final String prefix;

    ErrorType(String code) {
        this(code, "");
    }

    ErrorType(String code, String prefix) {
        this.code = code;
        this.prefix = prefix;
    }

    public String getCode() {
        return code;
    }

    public String getPrefix() {
        return prefix;
    }
}
