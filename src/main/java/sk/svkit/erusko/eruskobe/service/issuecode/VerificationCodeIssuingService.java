package sk.svkit.erusko.eruskobe.service.issuecode;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import sk.svkit.erusko.eruskobe.util.IssueCodeUtils;

import java.util.Collections;

@Service
public class VerificationCodeIssuingService {

    private static final Log LOGGER = LogFactory.getLog(VerificationCodeIssuingService.class);

    private static final ObjectReader OBJECT_READER = new ObjectMapper().readerFor(IssueCodeResponse.class);

    public static final String API_KEY_HEADER_NAME = "X-Api-Key";

    @Value("${VERIFICATION_SERVER_URL}")
    private String verificationServerUrl;

    private final RestTemplate restTemplate;

    public VerificationCodeIssuingService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public VerificationCode issueCode(IssueCodeDto issueCodeDto) {
        HttpEntity<IssueCodeRequest> requestEntity = new HttpEntity<>(IssueCodeUtils.convertIssueCodeDto2IssueCodeRequest(issueCodeDto),
                getHeaders(issueCodeDto.getApiKey()));

        IssueCodeResponse issueCodeResponse;
        try {
            issueCodeResponse = restTemplate.postForObject(verificationServerUrl + "/api/issue",
                    requestEntity, IssueCodeResponse.class);
            LOGGER.info("Succesfully issued verification code with uuid " + issueCodeResponse.getUuid());
        } catch (HttpClientErrorException e) {
            issueCodeResponse = createIssueCodeResponseFromHttpClientErrorException(e);
            LOGGER.warn("Verification code issuing failed with error: " + issueCodeResponse.getError());
        }

        return IssueCodeUtils.convertIssueCodeResponse2VerificationCode(issueCodeResponse);
    }

    private HttpHeaders getHeaders(String apiKey) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add(API_KEY_HEADER_NAME, apiKey);
        return headers;
    }

    private IssueCodeResponse createIssueCodeResponseFromHttpClientErrorException(HttpClientErrorException e) {
        if (StringUtils.isBlank(e.getResponseBodyAsString())) {
            return new IssueCodeResponse(e.getStatusText());
        }
        return tryToDeserializeClientErrorData(e);
    }

    private IssueCodeResponse tryToDeserializeClientErrorData(HttpClientErrorException e) {
        try {
            return OBJECT_READER.readValue(e.getResponseBodyAsString());
        } catch (JsonProcessingException jsonProcessingException) {
            LOGGER.error("Exception while deserializing issuing code error response", jsonProcessingException);
            return new IssueCodeResponse(e.getStatusText());
        }
    }
}
