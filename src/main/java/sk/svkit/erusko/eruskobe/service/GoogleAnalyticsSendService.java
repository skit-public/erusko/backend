package sk.svkit.erusko.eruskobe.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

/**
 * Service for sending events to Google analytics.
 */

@Service
public class GoogleAnalyticsSendService {

    @Value("${GOOGLE_ANALYTICS_TID}")
    private String trackingId;

    @Autowired
    private RestTemplate restTemplate;

    private static final Log LOGGER = LogFactory.getLog(GoogleAnalyticsSendService.class);

    public void sendData(final String action) {
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance();
        setupGoogleAnalyticsServerUri(action, builder);
        try {
            URI uri = builder.build().toUri();
            final HttpHeaders headers = new HttpHeaders();
            headers.set("user-agent", "Java");

            final HttpEntity<String> entity = new HttpEntity<>(headers);
            //response is ignored intentionally
            restTemplate.exchange(uri, HttpMethod.GET, entity, byte[].class);

            LOGGER.info("Google analytics data sent successfully. ");
        } catch (Exception e) {
            LOGGER.error("Unable to send analytics data.", e);
        }
    }

    private void setupGoogleAnalyticsServerUri(final String action, UriComponentsBuilder builder) {
        builder.scheme("https")
                .host("www.google-analytics.com")
                .path("/collect")
                .queryParam("v", "1") // API Version.
                .queryParam("tid", trackingId) // Tracking ID.
                .queryParam("cid", "2020") //Client id.
                .queryParam("t", "event") // Event hit type.
                .queryParam("ec", "erusko_backend") // Event category.
                .queryParam("ea", action); // Event action.
    }
}
