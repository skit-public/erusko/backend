package sk.svkit.erusko.eruskobe.service.smsgateway.request;

public class SmsSender {
    public SmsSender(String text) {
        this.text = text;
    }

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
