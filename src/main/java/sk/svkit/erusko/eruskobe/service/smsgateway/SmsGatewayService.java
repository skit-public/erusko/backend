package sk.svkit.erusko.eruskobe.service.smsgateway;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import sk.svkit.erusko.eruskobe.service.smsgateway.request.SmsRequest;
import sk.svkit.erusko.eruskobe.service.smsgateway.response.SmsResponse;
import sk.svkit.erusko.eruskobe.util.SmsRequestUtil;

@Service
public class SmsGatewayService {
    private static final Log LOGGER = LogFactory.getLog(SmsGatewayService.class);

    @Value("${SMS_GATEWAY_URL}")
    private String smsGatewayUrl;

    @Value("${SMS_GATEWAY_API_KEY}")
    private String smsGatewayApiKey;

    @Value("${SMS_BODY_TEMPLATE}")
    private String smsBodyTemplate;

    @Value("${SENDER_NAME}")
    private String senderName;

    private final RestTemplate restTemplate;

    @Autowired
    public SmsGatewayService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public SmsResultDto sendSMS(final SmsDataDto smsData) {
        HttpEntity<SmsRequest> request =
                new HttpEntity<>(SmsRequestUtil.createSmsRequest(smsData, smsBodyTemplate, smsGatewayApiKey, senderName),
                        getHttpHeaders());
        SmsResponse response = restTemplate.postForObject(smsGatewayUrl, request, SmsResponse.class);
        return SmsRequestUtil.mapResponseToDto(response);
    }

    private HttpHeaders getHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }
}
