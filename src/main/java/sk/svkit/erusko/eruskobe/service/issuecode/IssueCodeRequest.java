package sk.svkit.erusko.eruskobe.service.issuecode;

public class IssueCodeRequest {

    private String symptomDate;
    private String testDate;
    private String testType;
    private Integer tzOffset;
    private String phone;
    private String padding;

    public String getSymptomDate() {
        return symptomDate;
    }

    public void setSymptomDate(String symptomDate) {
        this.symptomDate = symptomDate;
    }

    public String getTestDate() {
        return testDate;
    }

    public void setTestDate(String testDate) {
        this.testDate = testDate;
    }

    public String getTestType() {
        return testType;
    }

    public void setTestType(String testType) {
        this.testType = testType;
    }

    public Integer getTzOffset() {
        return tzOffset;
    }

    public void setTzOffset(Integer tzOffset) {
        this.tzOffset = tzOffset;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPadding() {
        return padding;
    }

    public void setPadding(String padding) {
        this.padding = padding;
    }
}
