package sk.svkit.erusko.eruskobe.service.smsgateway.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SmsAuthData {
    @JsonProperty("apikey")
    private String apiKey;

    public SmsAuthData(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
