package sk.svkit.erusko.eruskobe.service.smsgateway.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SmsRequest {
    @JsonProperty("auth")
    private SmsAuthData apiKey;
    @JsonProperty("data")
    private SmsRequestData smsRequestData;

    public SmsAuthData getApiKey() {
        return apiKey;
    }

    public void setApiKey(SmsAuthData apiKey) {
        this.apiKey = apiKey;
    }

    public SmsRequestData getSmsRequestData() {
        return smsRequestData;
    }

    public void setSmsRequestData(SmsRequestData smsRequestData) {
        this.smsRequestData = smsRequestData;
    }
}
