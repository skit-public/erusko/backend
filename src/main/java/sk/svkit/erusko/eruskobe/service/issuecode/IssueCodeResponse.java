package sk.svkit.erusko.eruskobe.service.issuecode;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IssueCodeResponse {

    private UUID uuid;
    private String code;
    private Date expiresAt;
    private Long expiresAtTimestamp;
    private String longCode;
    private Date longExpiresAt;
    private Long longExpiresAtTimestamp;
    private String error;
    private String errorCode;
    private String padding;

    public IssueCodeResponse() {
    }

    public IssueCodeResponse(String error) {
        this.error = error;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(Date expiresAt) {
        this.expiresAt = expiresAt;
    }

    public Long getExpiresAtTimestamp() {
        return expiresAtTimestamp;
    }

    public void setExpiresAtTimestamp(Long expiresAtTimestamp) {
        this.expiresAtTimestamp = expiresAtTimestamp;
    }

    public String getLongCode() {
        return longCode;
    }

    public void setLongCode(String longCode) {
        this.longCode = longCode;
    }

    public Date getLongExpiresAt() {
        return longExpiresAt;
    }

    public void setLongExpiresAt(Date longExpiresAt) {
        this.longExpiresAt = longExpiresAt;
    }

    public Long getLongExpiresAtTimestamp() {
        return longExpiresAtTimestamp;
    }

    public void setLongExpiresAtTimestamp(Long longExpiresAtTimestamp) {
        this.longExpiresAtTimestamp = longExpiresAtTimestamp;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getPadding() {
        return padding;
    }

    public void setPadding(String padding) {
        this.padding = padding;
    }
}
