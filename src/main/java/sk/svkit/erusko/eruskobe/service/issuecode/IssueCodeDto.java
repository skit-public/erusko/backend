package sk.svkit.erusko.eruskobe.service.issuecode;

public class IssueCodeDto {

    private String apiKey;
    private String symptomDate;
    private String testDate;
    private String testType;
    private Integer tzOffset;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getSymptomDate() {
        return symptomDate;
    }

    public void setSymptomDate(String symptomDate) {
        this.symptomDate = symptomDate;
    }

    public String getTestDate() {
        return testDate;
    }

    public void setTestDate(String testDate) {
        this.testDate = testDate;
    }

    public String getTestType() {
        return testType;
    }

    public void setTestType(String testType) {
        this.testType = testType;
    }

    public Integer getTzOffset() {
        return tzOffset;
    }

    public void setTzOffset(Integer tzOffset) {
        this.tzOffset = tzOffset;
    }
}
