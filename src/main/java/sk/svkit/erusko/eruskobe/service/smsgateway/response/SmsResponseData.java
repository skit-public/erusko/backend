package sk.svkit.erusko.eruskobe.service.smsgateway.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SmsResponseData {
    @JsonProperty("batch_id")
    private String batchId;

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }
}
