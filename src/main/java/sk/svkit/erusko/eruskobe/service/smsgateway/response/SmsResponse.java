package sk.svkit.erusko.eruskobe.service.smsgateway.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SmsResponse {
    private String id;
    private SmsResponseData data;
    private String note;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SmsResponseData getData() {
        return data;
    }

    public void setData(SmsResponseData data) {
        this.data = data;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
