package sk.svkit.erusko.eruskobe.service.smsgateway.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class SmsRequestData {
    private String message;

    @JsonProperty("sender")
    private SmsSender smsSender;
    @JsonProperty("recipients")
    private List<SmsRecipient> smsRecipients;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SmsSender getSmsSender() {
        return smsSender;
    }

    public void setSmsSender(SmsSender smsSender) {
        this.smsSender = smsSender;
    }

    public List<SmsRecipient> getSmsRecipients() {
        return smsRecipients;
    }

    public void setSmsRecipients(List<SmsRecipient> smsRecipients) {
        this.smsRecipients = smsRecipients;
    }
}
