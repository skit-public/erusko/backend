package sk.svkit.erusko.eruskobe.service.smsgateway.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SmsRecipient {
    @JsonProperty("phonenr")
    private String phoneNumber;

    public SmsRecipient(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
