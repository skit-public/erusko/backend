package sk.svkit.erusko.eruskobe.util;

import org.apache.commons.lang3.StringUtils;
import sk.svkit.erusko.eruskobe.service.ErrorType;
import sk.svkit.erusko.eruskobe.service.issuecode.IssueCodeDto;
import sk.svkit.erusko.eruskobe.service.issuecode.IssueCodeRequest;
import sk.svkit.erusko.eruskobe.service.issuecode.IssueCodeResponse;
import sk.svkit.erusko.eruskobe.service.issuecode.VerificationCode;

import java.util.Objects;

public final class IssueCodeUtils {

    private IssueCodeUtils() {
    }

    public static IssueCodeRequest convertIssueCodeDto2IssueCodeRequest(IssueCodeDto issueCodeDto) {
        IssueCodeRequest result = new IssueCodeRequest();
        result.setSymptomDate(issueCodeDto.getSymptomDate());
        result.setTestDate(issueCodeDto.getTestDate());
        result.setTestType(issueCodeDto.getTestType());
        result.setTzOffset(issueCodeDto.getTzOffset());
        result.setPadding(PaddingGenerator.generate());
        return result;
    }

    public static VerificationCode convertIssueCodeResponse2VerificationCode(IssueCodeResponse issueCodeResponse) {
        VerificationCode result = new VerificationCode();
        result.setUuid(issueCodeResponse.getUuid());
        result.setCode(issueCodeResponse.getLongCode());
        result.setExpiresAt(issueCodeResponse.getExpiresAt());
        if (StringUtils.isNotBlank(issueCodeResponse.getError()) || StringUtils.isNotBlank(issueCodeResponse.getErrorCode())) {
            result.setError(ErrorType.VC.getPrefix() + Objects.toString(issueCodeResponse.getError(), ""));
            result.setErrorCode(ErrorType.VC.getCode() + Objects.toString(issueCodeResponse.getErrorCode(), ""));
        }
        return result;
    }
}
