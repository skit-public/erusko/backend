package sk.svkit.erusko.eruskobe.util;

import java.util.Base64;
import java.util.Random;

public final class PaddingGenerator {

    public static final int PADDING_BOUND = 1024;
    public static final int PADDING_MINIMAL_LENGTH = 1024;

    private PaddingGenerator() {
    }

    /**
     * Generates and returns 1-2kb (random) of base64-encoded bytes.
     */
    public static String generate() {
        Random random = new Random();
        int size = random.nextInt(PADDING_BOUND);
        size += PADDING_MINIMAL_LENGTH;
        byte[] bytes = new byte[size];
        random.nextBytes(bytes);
        return Base64.getEncoder().encodeToString(bytes);
    }
}
