package sk.svkit.erusko.eruskobe.util;

import org.apache.commons.lang3.Range;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.text.StringSubstitutor;
import sk.svkit.erusko.eruskobe.service.ErrorType;
import sk.svkit.erusko.eruskobe.service.smsgateway.SmsDataDto;
import sk.svkit.erusko.eruskobe.service.smsgateway.SmsResultDto;
import sk.svkit.erusko.eruskobe.service.smsgateway.request.*;
import sk.svkit.erusko.eruskobe.service.smsgateway.response.SmsResponse;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public final class SmsRequestUtil {
    private static final Log LOG = LogFactory.getLog(SmsRequestUtil.class);

    public static final String VC_EXPIRATION_TIME_FORMAT = "dd.MM. HH:mm";
    private static final int MIN_YEAR = 1900;

    private SmsRequestUtil() {
    }

    public static SmsRequest createSmsRequest(final SmsDataDto data,
                                              final String smsTemplate,
                                              final String smsGatewayApiKey,
                                              final String senderName) {
        SmsRequest request = new SmsRequest();
        request.setApiKey(new SmsAuthData(smsGatewayApiKey));
        if (null != smsTemplate) {
            request.setSmsRequestData(createSmsData(data, smsTemplate, senderName));
        } else {
            throw new RuntimeException("SMS template not set.");
        }
        return request;
    }

    private static SmsRequestData createSmsData(final SmsDataDto data,
                                                final String smsTemplate,
                                                final String senderName) {
        SmsRequestData result = new SmsRequestData();
        if (data != null) {
            result.setMessage(fillTemplate(smsTemplate, data));
            result.setSmsRecipients(Collections.singletonList(new SmsRecipient(data.getPhoneNumber())));
        }
        fillSender(senderName, result);
        return result;
    }

    private static void fillSender(String senderName, SmsRequestData result) {
        //default is UVZ
        if (StringUtils.isEmpty(senderName)) {
            result.setSmsSender(new SmsSender("UVZ"));
        } else {
            result.setSmsSender(new SmsSender(senderName));
        }
    }

    private static String fillTemplate(final String smsTemplate, final SmsDataDto data) {
        Map<String, String> values = new HashMap<>();
        if (data.getFirstname() != null) {
            values.put("NAME_FIRST_CHARACTER", getFirstLetter(data.getFirstname()));
        }
        if (data.getLastname() != null) {
            values.put("SURNAME_FIRST_CHARACTER", getFirstLetter(data.getLastname()));
        }
        values.put("BIRTHDATE_YEAR", getYear(data.getBirthdayYear()));

        if (data.getVerificationCode() != null) {
            values.put("VERIFICATION_CODE", data.getVerificationCode());
        }
        if (data.getVerificationCodeExpiration() != null) {
            values.put("VERIFICATION_CODE_EXPIRY_DATE", formatDate(data.getVerificationCodeExpiration()));
        }
        return StringSubstitutor.replace(smsTemplate, values, "{", "}");
    }

    private static String getYear(final Integer birthdayYear) {
        String result = "";
        int year = Year.now().getValue();
        Range<Integer> myRange = Range.between(MIN_YEAR, year);
        if (myRange.contains(birthdayYear)) {
            result = " (" + birthdayYear.toString() + ")";
        }
        return result;
    }

    private static String getFirstLetter(final String value) {
        String withoutWhitespaces = value.trim();
        if (withoutWhitespaces.isEmpty()) {
            return "";
        }
        return " " + withoutWhitespaces.substring(0, 1).toUpperCase() + ".";
    }

    private static String formatDate(Date date) {
        String result = "";
        if (date != null) {
            DateFormat dateFormat = new SimpleDateFormat(VC_EXPIRATION_TIME_FORMAT);
            result = dateFormat.format(date);
        }
        return result;
    }

    public static SmsResultDto mapResponseToDto(final SmsResponse response) {
        final SmsResultDto result = new SmsResultDto();
        String smsStatus = response.getId();
        if (smsStatus.equalsIgnoreCase("OK")) {
            result.setBatchId(response.getData().getBatchId());
            LOG.info("Successfully sent SMS with ID: " + response.getData().getBatchId());
        } else {
            result.setError(getErrorMessage(response, smsStatus));
            result.setErrorCode(ErrorType.SG.getCode());
            LOG.error("Send SMS failed with error: " + smsStatus);
        }
        return result;
    }

    private static String getErrorMessage(SmsResponse response, String smsStatus) {
        if (response.getNote() != null) {
            return ErrorType.SG.getPrefix() + smsStatus + " " + response.getNote();
        } else {
            return ErrorType.SG.getPrefix() + smsStatus;
        }
    }
}
