package sk.svkit.erusko.eruskobe.validation;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import sk.svkit.erusko.eruskobe.service.issuecode.VerificationCodeIssuingService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;


public class PhoneNumberValidator implements ConstraintValidator<PhoneNumber, String> {

    private static final Log LOGGER = LogFactory.getLog(VerificationCodeIssuingService.class);
    private static final String NUMBER_REGEXP = "^([+\\d\\s])+";
    private static final Pattern PATTERN = Pattern.compile(NUMBER_REGEXP);

    @Override
    public void initialize(PhoneNumber constraintAnnotation) {
    }

    @Override
    public boolean isValid(String phoneNumber, ConstraintValidatorContext constraintValidatorContext) {
        boolean result = false;
        if (null != phoneNumber && PATTERN.matcher(phoneNumber).matches()) {
            try {
                PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
                Phonenumber.PhoneNumber pn = phoneNumberUtil.parse(phoneNumber, "SK");
                if (pn != null) {
                    result = phoneNumberUtil.isValidNumber(pn);
                }
            } catch (NumberParseException e) {
                LOGGER.info("Can not parse number: " + phoneNumber);
            }
        } else {
            return false;
        }
        return result;
    }
}
