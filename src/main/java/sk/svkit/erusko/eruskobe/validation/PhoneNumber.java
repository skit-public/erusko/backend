package sk.svkit.erusko.eruskobe.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = PhoneNumberValidator.class)
public @interface PhoneNumber {
    String message() default "Format not valid";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };
}
