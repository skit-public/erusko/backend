package sk.svkit.erusko.eruskobe.handler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import sk.svkit.erusko.eruskobe.controller.dto.ApiResponse;
import sk.svkit.erusko.eruskobe.service.ErrorType;

import java.util.stream.Collectors;

@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
public class RestResponseEntityExceptionHandler {

    private static final Log LOGGER = LogFactory.getLog(RestResponseEntityExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleInternalServerError(Exception e) {
        LOGGER.error(e.getMessage(), e);
        ApiResponse response = new ApiResponse(e.getMessage(), ErrorType.INTERNAL_ERROR.getCode());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleValidationExceptions(MethodArgumentNotValidException e) {
        LOGGER.info("Validation exception: {}", e);
        String errors = e.getBindingResult().getAllErrors().stream()
                .map(error -> {
                    String errorDesc = ((FieldError) error).getField() + ": ";
                    errorDesc += error.getDefaultMessage();
                    return errorDesc;
                })
                .collect(Collectors.joining("; "));
        ApiResponse response = new ApiResponse(errors, ErrorType.BAD_REQUEST.getCode());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }
}
