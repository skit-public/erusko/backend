package sk.svkit.erusko.eruskobe.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sk.svkit.erusko.eruskobe.controller.dto.IssueApiRequest;
import sk.svkit.erusko.eruskobe.controller.dto.IssueApiResponse;
import sk.svkit.erusko.eruskobe.event.AnalyticsEvent;
import sk.svkit.erusko.eruskobe.event.AnalyticsEventType;
import sk.svkit.erusko.eruskobe.service.issuecode.IssueCodeDto;
import sk.svkit.erusko.eruskobe.service.issuecode.VerificationCode;
import sk.svkit.erusko.eruskobe.service.issuecode.VerificationCodeIssuingService;
import sk.svkit.erusko.eruskobe.service.smsgateway.SmsDataDto;
import sk.svkit.erusko.eruskobe.service.smsgateway.SmsGatewayService;
import sk.svkit.erusko.eruskobe.service.smsgateway.SmsResultDto;
import sk.svkit.erusko.eruskobe.util.PaddingGenerator;

import javax.validation.Valid;

@RestController
@Validated
@RequestMapping(value = "/api")
public class IssuingController {

    public static final String API_KEY_HEADER = "X-Api-Key";
    private final VerificationCodeIssuingService verificationCodeIssuingService;
    private final SmsGatewayService smsGatewayService;
    private final ApplicationEventPublisher eventPublisher;

    @Autowired
    public IssuingController(VerificationCodeIssuingService verificationCodeIssuingService, SmsGatewayService smsGatewayService,
                             ApplicationEventPublisher eventPublisher) {
        this.verificationCodeIssuingService = verificationCodeIssuingService;
        this.smsGatewayService = smsGatewayService;
        this.eventPublisher = eventPublisher;
    }

    @PostMapping(path = "/issue", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<IssueApiResponse> issue(@RequestHeader(API_KEY_HEADER) String apiKey,
                                                  @RequestBody @Valid IssueApiRequest request) {
        eventPublisher.publishEvent(new AnalyticsEvent(AnalyticsEventType.server_vc_request.name()));
        IssueApiResponse response = new IssueApiResponse();
        response.setPadding(PaddingGenerator.generate());
        VerificationCode verificationCode = verificationCodeIssuingService.issueCode(createIssueCodeDto(request, apiKey));
        if (StringUtils.isNotBlank(verificationCode.getError()) || StringUtils.isNotBlank(verificationCode.getErrorCode())) {
            response.setError(verificationCode.getError());
            response.setErrorCode(verificationCode.getErrorCode());
            eventPublisher.publishEvent(new AnalyticsEvent(AnalyticsEventType.server_vc_error.name()));
            return ResponseEntity.badRequest().body(response);
        }
        response.setCodeUuid(verificationCode.getUuid());
        response.setExpiresAt(verificationCode.getExpiresAt());

        SmsResultDto smsResultDto = smsGatewayService.sendSMS(createSmsDataDto(request, verificationCode));
        if (smsResultDto.getBatchId() != null) {
            response.setSmsBatchId(smsResultDto.getBatchId());
        } else if (smsResultDto.getErrorCode() != null) {
            response.setErrorCode(smsResultDto.getErrorCode());
            response.setError(smsResultDto.getError());
            eventPublisher.publishEvent(new AnalyticsEvent(AnalyticsEventType.server_vc_error.name()));
            return ResponseEntity.badRequest().body(response);
        }
        eventPublisher.publishEvent(new AnalyticsEvent(AnalyticsEventType.server_vc_success.name()));
        return ResponseEntity.ok(response);
    }

    private IssueCodeDto createIssueCodeDto(IssueApiRequest request, String apiKey) {
        IssueCodeDto result = new IssueCodeDto();
        result.setApiKey(apiKey);
        result.setSymptomDate(request.getSymptomDate());
        result.setTestDate(request.getTestDate());
        result.setTestType(request.getTestType());
        result.setTzOffset(request.getTzOffset());
        return result;
    }

    private SmsDataDto createSmsDataDto(IssueApiRequest request, VerificationCode verificationCode) {
        SmsDataDto result = new SmsDataDto();
        result.setFirstname(request.getFirstNameCharacter());
        result.setLastname(request.getLastNameCharacter());
        result.setBirthdayYear(request.getBirthYear());
        result.setVerificationCode(verificationCode.getCode());
        result.setVerificationCodeExpiration(verificationCode.getExpiresAt());
        result.setPhoneNumber(request.getPhone());
        return result;
    }
}
