package sk.svkit.erusko.eruskobe.controller.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;
import java.util.UUID;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class IssueApiResponse extends ApiResponse {

    private UUID codeUuid;
    private Date expiresAt;
    private String smsBatchId;
    private String padding;

    public IssueApiResponse() {
    }

    public UUID getCodeUuid() {
        return codeUuid;
    }

    public void setCodeUuid(UUID codeUuid) {
        this.codeUuid = codeUuid;
    }

    public Date getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(Date expiresAt) {
        this.expiresAt = expiresAt;
    }

    public String getSmsBatchId() {
        return smsBatchId;
    }

    public void setSmsBatchId(String smsBatchId) {
        this.smsBatchId = smsBatchId;
    }

    public String getPadding() {
        return padding;
    }

    public void setPadding(String padding) {
        this.padding = padding;
    }
}
