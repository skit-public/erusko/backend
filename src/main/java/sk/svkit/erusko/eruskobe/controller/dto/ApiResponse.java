package sk.svkit.erusko.eruskobe.controller.dto;

public class ApiResponse {

    private String error;
    private String errorCode;

    public ApiResponse() {
    }

    public ApiResponse(String error, String errorCode) {
        this.error = error;
        this.errorCode = errorCode;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
