package sk.svkit.erusko.eruskobe.controller.dto;

import sk.svkit.erusko.eruskobe.validation.PhoneNumber;

public class IssueApiRequest {

    private String firstNameCharacter;
    private String lastNameCharacter;
    private Integer birthYear;
    private String symptomDate;
    private String testDate;
    private String testType;
    private Integer tzOffset;
    @PhoneNumber
    private String phone;
    private String padding;

    public String getFirstNameCharacter() {
        return firstNameCharacter;
    }

    public void setFirstNameCharacter(String firstNameCharacter) {
        this.firstNameCharacter = firstNameCharacter;
    }

    public String getLastNameCharacter() {
        return lastNameCharacter;
    }

    public void setLastNameCharacter(String lastNameCharacter) {
        this.lastNameCharacter = lastNameCharacter;
    }

    public Integer getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(Integer birthYear) {
        this.birthYear = birthYear;
    }

    public String getSymptomDate() {
        return symptomDate;
    }

    public void setSymptomDate(String symptomDate) {
        this.symptomDate = symptomDate;
    }

    public String getTestDate() {
        return testDate;
    }

    public void setTestDate(String testDate) {
        this.testDate = testDate;
    }

    public String getTestType() {
        return testType;
    }

    public void setTestType(String testType) {
        this.testType = testType;
    }

    public Integer getTzOffset() {
        return tzOffset;
    }

    public void setTzOffset(Integer tzOffset) {
        this.tzOffset = tzOffset;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPadding() {
        return padding;
    }

    public void setPadding(String padding) {
        this.padding = padding;
    }
}
