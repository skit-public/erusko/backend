#!/bin/bash

# Change the prefix is required.
# It has to be compliant with sed command, line 14
secret_prefix="secret:"
##env
##gcloud config list
##gcloud config set project ${PROJECT_ID}

#Project ID check:
USER_PROJECT_ID=$(gcloud config get-value project)
if [ "${PROJECT_ID}" != "${USER_PROJECT_ID}" ]; then
  echo "✋ User project in CLI: gcloud config is '${USER_PROJECT_ID}', but shoud be '${PROJECT_ID}'!" >&2
  exit 1
fi

# load the secrets
for i in $(printenv | grep ${secret_prefix})
do
  key=$(echo ${i} | cut -d'=' -f 1)
  val=$(echo ${i} | cut -d'=' -f 2-)
  if [[ ${val} == ${secret_prefix}* ]]
  then
    projectNum=$(echo ${val} | cut -d'/' -f 4)
    secret=$(echo ${val} | cut -d'/' -f 3-)

    if [[ -n ${PROJECT_ID} ]]
    then
      project="--project=${PROJECT_ID}"
    fi

    secretName=$(echo ${secret} | cut -d'/' -f 4)
    version=$(echo ${secret} | cut -d'/' -f 6)
    #Check access for given secret:
    gserviceaccount="${projectNum}-compute@developer.gserviceaccount.com"
    if [ $(gcloud beta secrets get-iam-policy ${secretName} | grep -c ${gserviceaccount}) != "1" ]; then
      echo "✋ It looks that Gservice account '${gserviceaccount}' has no access to secret '${secretName}'" >&2
      exit 1
    fi

    ##echo $key, $val, $projectId, $secret, $secretName, $version, $project, $gserviceaccount
    plain="$(gcloud beta secrets versions access --secret=${secretName} ${version})"
    ##echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> gcloud beta secrets versions access --secret=${secretName} ${version}"
    #For multiline management
    export $key=$(echo $plain | sed -e 's/\n//g' | sed 's/"//g')
    ##echo ">>>$key: ${!key}" #${${key}} not working
  fi
done
#run the following command
$@
