echo ">>>>> Total size copied $(du -hs)"
mvn package -DskipTests
echo ">>>>> Total size of workspace after build $(du -hs)"
cd target && mv *.jar er_backend.jar
echo ">>>>> Target JAR:"
ls -lh er_backend.jar
