# Backend part of eRusko application

- Java 11
- Maven

##Build project
- Build project, skip all tests: 
  - `mvn clean install -DskipTests`

##Logs setup
Logs file and directory can be set with Spring Boot properties:
- If both `logging.file` and `logging.path` are NOT set - console only logging,
- `logging.file` - log file name (exact location or relative path to current directory),
- `logging.path` - log directory (exact location or relative path to current directory).

##Mandatory ENV variables. 
To run application you need to set up following mandatory environment variables.
 - SMS_GATEWAY_API_KEY
 - GOOGLE_ANALYTICS_TID
 - DEEPLINK_HOSTNAME
 - INFO_URL
 - VERIFICATION_SERVER_URL

##Local development 
For local development use LOCAL profile. You don't need to set up mandatory variables.
To run app from project dir use command: `java -jar -Dspring.profiles.active=local target/er-backend-0.0.1-SNAPSHOT.jar`